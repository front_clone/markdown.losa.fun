const keyboard = new Map([
  [
    "insertText",
    (event, data) => {
      console.log(event.data);
    },
  ],
  [
    "insertCompositionText",
    (event, data) => {
      if (event.data == "测试") {
        event.target.innerHTML += `<strong>${event.data}</strong>`;
      }
    },
  ],
  ["deleteContentBackward", (event, data) => {}],
  ["insertFromPaste", (event, data) => {}],
  ["deleteByCut", (event, data) => {}],
  ["insertParagraph", (event, data) => {}],
]);

class KeyboardEventHandler {
  isComposing = false;

  constructor(el, editor) {
    this.editor = editor;
    this.addEventListener(el);
  }

  inputHandler(event) {
    setTimeout(() => {
      if (this.isComposing) return;
      const { inputType, target } = event;
      // console.log('dataTransfer', event.dataTransfer);
      // const fnCall = keyboard.get(inputType);
      // console.log(inputType, target.innerHTML);
      // // this.editor.updataModelView();
      // if (fnCall) fnCall.call(this, event);
      // console.log(marked(target.innerHTML));
    }, 0);
  }

  compositionStart(event) {
    this.isComposing = true;
  }

  compositionEnd(event) {
    this.isComposing = false;
  }

  addEventListener(el) {
    el.addEventListener("compositionend", this.compositionEnd.bind(this));
    el.addEventListener("input", this.inputHandler.bind(this));
    el.addEventListener("compositionstart", this.compositionStart.bind(this));
  }
}

export default KeyboardEventHandler;
