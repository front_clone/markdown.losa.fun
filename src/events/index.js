import KeyboardEventHandler from "./keyboard.js";

class EventElHandler {
  constructor(el, editor) {
    this.initState(el, editor);
  }

  initState(el) {
    new KeyboardEventHandler(el, editor);
  }
}

export default EventElHandler;
