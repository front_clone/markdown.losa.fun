import EventElHandler from "../events/index.js";

class Editor {
  constructor(el, options) {
    this.el = el;
    this.options = options;
    this.initState();
  }

  render(h) {}

  $emit() {}

  $on() {}

  initState() {
    new EventElHandler(this.el, this);
  }

  updataModelView () {
    
  }
}

export default Editor;
