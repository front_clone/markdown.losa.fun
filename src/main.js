// import Editor from "./editor/index.js";
// const el = document.getElementById("editor");
// const editor = new Editor(el, {});
// console.log("===== editor run =====", editor);
// setInterval(() => {
//   console.log(document.createTreeWalker(el));
// }, 1000);

class PnlCode {
  start = 0;
  ctx = null;
  constructor(src, first = 0) {
    this.start = first;
    this.src = src;
    const sln = src.slice(first);
    if (/^. /.test(sln)) {
      const start = first + 2;
      this.ctx = new PnlCode(src, start);
    } else {
      console.log(src, sln);
    }
  }
}

const npl = new PnlCode("> - 12**34**56***");
console.log(npl);
const normal = "12***34**56~cs##++";
// const normal = "12***34**56~cs~埋点";

function lineParse(line = "") {
  let queue = [],
    tmpls = "";
  const dels = ["*", "~"];

  for (let i = 0; i < line.length; i++) {
    let valStr = line[i];
    if (dels.includes(valStr)) {
      queue.push(tmpls);
      tmpls = valStr;
      while (dels.includes(line[i + 1])) {
        i += 1;
        tmpls += line[i];
      }
      queue.push(tmpls);
      tmpls = "";
    } else {
      tmpls += valStr;
    }
  }

  if (tmpls.length) queue.push(tmpls);

  return queue;
}
console.log(normal);
console.log(lineParse(normal));
