import BasicRender from "./index.js";

class SourceView extends BasicRender {
  constructor(textModel, selectionModel) {
    super(textModel, selectionModel);
  }
}

export default SourceView;
