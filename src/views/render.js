import BasicRender from "./index.js";

class RenderView extends BasicRender {
  constructor(textModel, selectionModel) {
    super(textModel, selectionModel);
  }
}

export default RenderView;
